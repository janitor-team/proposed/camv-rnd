#include <stdio.h>
#include <string.h>
#include "arg.h"
#include "log.h"
#include "dep.h"
#include "db.h"
#include "libs.h"
#include "tmpasm_scconfig.h"
#include "Rev.h"
#include "../util/arg_auto_set.h"

#include <librnd/src_3rd/libfungw/scconfig_hooks.h>

/* we are doing /local/camv/ */
#define LIBRND_SCCONFIG_APP_TREE "camv"

#include <librnd/src_3rd/puplug/scconfig_hooks.h>
#include <librnd/scconfig/plugin_3state.h>
#include <librnd/scconfig/hooks_common.h>
#include <librnd/scconfig/rnd_hook_detect.h>

#define version "1.1.0"

#define REQ_LIBRND_MAJOR 3
#define REQ_LIBRND_MINOR 2


const arg_auto_set_t disable_libs[] = { /* list of --disable-LIBs and the subtree they affect */
	{"enable-byaccic",    "/local/camv/want_byaccic",      arg_true,      "$enable generating language files using byaccic/ureglex"},
	{"disable-byaccic",   "/local/camv/want_byaccic",      arg_false,     "$disable generating language files byaccic/ureglex"},

#undef plugin_def
#undef plugin_header
#undef plugin_dep
#define plugin_def(name, desc, default_, all_) plugin3_args(name, desc)
#define plugin_header(sect)
#define plugin_dep(plg, on)
#include "plugins.h"

	{NULL, NULL, NULL, NULL}
};

#define TO_STR_(payload) #payload
#define TO_STR(payload) TO_STR_(payload)


static void help1(void)
{
	rnd_help1("camv-rnd");
	printf(" --dot_camv_rnd=path        .camv-rnd config path under $HOME/\n");
	printf(" --fontdir=path             system font dir that hosts ttf files\n");
}

/* Runs when a custom command line argument is found
 returns true if no furhter argument processing should be done */
int hook_custom_arg(const char *key, const char *value)
{
	rnd_hook_custom_arg(key, value, disable_libs); /* call arg_auto_print_options() instead */

	if (strcmp(key, "dot_camv_rnd") == 0) {
		need_value("use --dot_camv_rnd=dir");
		put("/local/camv/dot_camv_rnd", value);
		return 1;
	}
	if (strcmp(key, "fontdir") == 0) {
		need_value("use --fontdir=path");
		put("/local/camv/fontdir", value);
		return 1;
	}

	if (arg_auto_set(key, value, disable_libs) == 0) {
		fprintf(stderr, "Error: unknown argument %s\n", key);
		exit(1);
	}
	return 1; /* handled by arg_auto_set() */

}


/* Runs before anything else */
int hook_preinit()
{
	return 0;
}

void librnd_ver_req_min(int exact_major, int min_minor);

/* Runs after initialization */
int hook_postinit()
{
	db_mkdir("/local");
	db_mkdir("/local/camv");
	rnd_hook_postinit();

	/* defaults */
	put("/local/camv/debug", sfalse);
	put("/local/camv/profile", sfalse);

	put("/local/camv/librnd_prefix", TO_STR(LIBRND_PREFIX));
	put("/local/camv/dot_camv_rnd", ".camv-rnd");

	librnd_ver_req_min(REQ_LIBRND_MAJOR, REQ_LIBRND_MINOR);

	return 0;
}

/* Runs after all arguments are read and parsed */
int hook_postarg()
{
	char *tmp;
	const char *libad;

	put("/local/camv/librnd_template", tmp = str_concat("", TO_STR(LIBRND_PREFIX), "/", get("/local/libarchdir"), "/librnd3/scconfig/template", NULL));
	free(tmp);

	/* if librnd is installed at some custom path, we'll need to have a -I on CFLAGS and -L on LDFLAGS */
	libad = get("/local/libarchdir");
	if (((strncasecmp(TO_STR(LIBRND_PREFIX), "/usr/include", 12) != 0) && (strncasecmp(TO_STR(LIBRND_PREFIX), "/usr/local/include", 18) != 0)) || (strcmp(libad, "lib") != 0)) {
		put("/local/camv/librnd_extra_inc", "-I" TO_STR(LIBRND_PREFIX) "/include");
		put("/local/camv/librnd_extra_ldf", tmp = str_concat("", "-L" TO_STR(LIBRND_PREFIX) "/", libad, NULL));
		free(tmp);
	}


	return rnd_hook_postarg(TO_STR(LIBRND_PREFIX), "camv-rnd");
}

/* Runs when things should be detected for the host system */
int hook_detect_host()
{
	return rnd_hook_detect_host();
}

#include "find_fontdir.c"

/* Runs when things should be detected for the target system */
int hook_detect_target()
{
	const char *host_ansi, *host_ped, *target_ansi, *target_ped, *target_pg, *target_no_pie;

	require("cc/cc", 0, 1);
	require("cc/fpic",  0, 0);

	rnd_hook_detect_cc();
	if (rnd_hook_detect_sys() != 0)
		return 1;

	/* detect inline after fixing up the flags */
	require("cc/inline", 0, 0);

	require("sys/types/size/4_u_int", 0, 1);

	require("libs/sul/librnd-hid/*", 0, 1);
	require("libs/sul/freetype2/*", 0, 1);

	/* byaccic - are we able to regenerate languages? */
	if (istrue(get("/local/camv/want_byaccic"))) {
		require("parsgen/byaccic/*", 0, 0);
		require("parsgen/ureglex/*", 0, 0);
		if (!istrue(get("parsgen/byaccic/presents")) || !istrue(get("parsgen/ureglex/presents")))
			put("/local/camv/want_parsgen_byaccic", sfalse);
		else
			put("/local/camv/want_parsgen_byaccic", strue);
	}
	else {
		report("byaccic/ureglex are disabled, along with parser generation.\n");
		put("/local/camv/want_parsgen_byaccic", sfalse);
	}

	if (find_fontdir() != 0)
		return 1;

	return 0;
}

static const char *IS_OK(int *generr, int val)
{
	*generr |= val;
	if (val == 0) return "ok";
	return "ERROR";
}

/* Runs after detection hooks, should generate the output (Makefiles, etc.) */
int hook_generate()
{
	int generr = 0;
	char *tmp, *rev = "non-svn";

	tmp = svn_info(0, "..", "Revision:");
	if (tmp != NULL) {
		rev = str_concat("", "svn r", tmp, NULL);
		free(tmp);
	}

	logprintf(0, "scconfig generate version info: version='%s' rev='%s'\n", version, rev);
	put("/local/revision", rev);
	put("/local/version",  version);

	put("/local/pup/sccbox", "../../scconfig/sccbox");

	printf("Generating config.h... %s\n", IS_OK(&generr, tmpasm(NULL, "../config.h.in", "../config.h")));
	printf("Generating config.sh... %s\n", IS_OK(&generr, tmpasm(NULL, "../config.sh.in", "../config.sh")));
	printf("Generating Makefile.conf... %s\n", IS_OK(&generr, tmpasm(NULL, "../Makefile.conf.in", "../Makefile.conf")));
	printf("Generating src/Makefile... %s\n", IS_OK(&generr, tmpasm(NULL, "../src/Makefile.in", "../src/Makefile")));


	if (!generr) {
		printf("\n\n");
		printf("=====================\n");
		printf("Configuration summary\n");
		printf("=====================\n");

		print_sum_setting("/local/camv/debug",          "Compilation for debugging");
		print_sum_setting_or("/local/camv/symbols",     "Include debug symbols", istrue(get("/local/camv/debug")));
		print_sum_cfg_val("/local/prefix",              "installation prefix (--prefix)");
		print_sum_cfg_val("/local/confdir",             "configuration directory (--confdir)");
		print_sum_cfg_val("/local/camv/dot_camv_rnd",   ".camv_rnd config dir under $HOME");
		print_sum_cfg_val("/local/camv/fontdir",        "system font dir");

#undef plugin_def
#undef plugin_header
#undef plugin_dep
#define plugin_def(name, desc, default_, all_) plugin3_stat(name, desc)
#define plugin_header(sect) printf(sect);
#define plugin_dep(plg, on)
#include "plugins.h"

		if (all_plugin_check_explicit()) {
			printf("\nNevertheless the configuration is complete, if you accept these differences\nyou can go on compiling.\n\n");
			generr = 1;
		}
		else
			printf("\nConfiguration complete, ready to compile.\n\n");


		{
			FILE *f;
			f = fopen("Rev.stamp", "w");
			fprintf(f, "%d", myrev);
			fclose(f);
		}

	}
	else
		fprintf(stderr, "\nError generating some of the files\n");

	return generr;
}

/* Runs before everything is uninitialized */
void hook_preuninit()
{
}

/* Runs at the very end, when everything is already uninitialized */
void hook_postuninit()
{
}

