static const char *font_dirs[] = {
	"/usr/share/fonts/truetype",
	"/usr/share/fonts",
	"/usr/local/share/fonts/truetype",
	"/usr/local/share/fonts",
	NULL
};

int find_fontdir(void)
{
	const char **d, *ud;

	ud = get("/local/csch/fontdir");
	if ((ud != NULL) && (*ud != '\0')) {
		report("Accepting system font dir (user specified): '%s'\n", ud);
		return 0;
	}

	report("Detecting system font dir...");
	for(d = font_dirs; *d != NULL; d++) {
		if (is_dir(*d)) {
			put("/local/csch/fontdir", *d);
			report("'%s'\n", *d);
			return 0;
		}
	}
	report("NOT FOUND!\nPlease specify one with --fontdir\n");
	return 1;
}
