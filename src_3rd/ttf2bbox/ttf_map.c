#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <ctype.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>
#include <genht/hash.h>

#include <ft2build.h>
#include FT_FREETYPE_H

#include "ttf_map.h"

#ifndef TTF_PATH_MAX
#include <limits.h>
#ifdef PATH_MAX
#define TTF_PATH_MAX PATH_MAX
#else
#define TTF_PATH_MAX 8192
#endif
#endif

char ttf_map_cache_uniq[8];

char *ttf_strdup(const char *s)
{
	int l = strlen(s);
	char *o = malloc(l+1);
	memcpy(o, s, l+1);
	return o;
}

ttf_map_font_t *ttf_map_get(htsp_t *map, const char *path, int alloc)
{
	ttf_map_font_t *f = htsp_get(map, path);

	if ((f == NULL) && alloc) {
		f = calloc(sizeof(ttf_map_font_t), 1);
		f->path = ttf_strdup(path);
		htsp_set(map, (char *)f->path, f);
	}

	return f;
}

#define REQ_FONT \
	if (font == NULL) { \
		fprintf(stderr, "%s:%ld: syntax error (%s needs to be in a font)\n", cache_path, lineno, cmd); \
		goto error; \
	}

int ttf_map_cache_load(htsp_t *map, const char *cache_path)
{
	char *end, *cmd, *arg, *line, tmp[TTF_PATH_MAX+128];
	FILE *f = fopen(cache_path, "r");
	long lineno = 0;
	int res = -1;
	ttf_map_font_t *font = NULL;

	if (f == NULL)
		return -1;

	while((line = fgets(tmp, sizeof(tmp), f)) != NULL) {
		lineno++;

		/* strip line */
		while(isspace(*line)) line++;
		end = line + strlen(line) - 1;
		while((end >= line) && ((*end == '\n') || (*end == '\r'))) {
			*end = '\0';
			end--;
		}

		/* skip empty and comment */
		if ((*line == '\0') || (*line == '#')) continue;

		/* split into cmd and arg */
		cmd = line;
		arg = strchr(cmd, ' ');
		if (arg == NULL) {
			fprintf(stderr, "%s:%ld: syntax error (missing arg sep)\n", cache_path, lineno);
			goto error;
		}
		*arg = '\0';
		arg++;
		while(isspace(*arg)) arg++;

		/* parse command */
		if (strcmp(cmd, "font") == 0) {
			if (font != NULL) {
				fprintf(stderr, "%s:%ld: syntax error (previous font not closed)\n", cache_path, lineno);
				goto error;
			}
			font = ttf_map_get(map, arg, 1);
			if (font == NULL) {
				fprintf(stderr, "%s:%ld: failed to allocate font slot\n", cache_path, lineno);
				goto error;
			}
		}
		else if (strcmp(cmd, "end") == 0) {
			if (strcmp(arg, "font") != 0) {
				fprintf(stderr, "%s:%ld: syntax error (end must have arg=font, not '%s')\n", cache_path, lineno, arg);
				goto error;
			}
			else
				font = NULL;
		}
		else if (strcmp(cmd, "mtime") == 0) {
			REQ_FONT;
			font->mtime = strtoul(arg, &end, 10);
			if (*end != '\0') {
				fprintf(stderr, "%s:%ld: syntax error (invalid decimal integer '%s')\n", cache_path, lineno, arg);
				goto error;
			}
		}
		else if (strcmp(cmd, "is_font") == 0) {
			font->is_font = (*arg == '1');
		}
		else if (strcmp(cmd, "family") == 0) {
			REQ_FONT;
			font->family_name = ttf_strdup(arg);
		}
		else if (strcmp(cmd, "style") == 0) {
			REQ_FONT;
			font->style_name = ttf_strdup(arg);
		}
		else {
			fprintf(stderr, "%s:%ld: syntax error (invalid command '%s')\n", cache_path, lineno, cmd);
			goto error;
		}
	}

	if (font != NULL) {
		fprintf(stderr, "%s:%ld: syntax error (last font not closed)\n", cache_path, lineno);
		goto error;
	}

	res = 0;
	error:;
	fclose(f);
	return res;
}

int ttf_map_cache_save(htsp_t *map, const char *cache_path)
{
	FILE *f;
	htsp_entry_t *e;
	char *end, tmp[TTF_PATH_MAX];
	long len = strlen(cache_path);

	if (len >= TTF_PATH_MAX - 16)
		return -1;

	memcpy(tmp, cache_path, len);
	end = tmp + len;
	strcpy(end, ".tmp");
	end += 4;

	if (*ttf_map_cache_uniq != '\0')
		strcpy(end, ttf_map_cache_uniq);


	f = fopen(tmp, "w");
	for(e = htsp_first(map); e != NULL; e = htsp_next(map, e)) {
		ttf_map_font_t *font = e->value;
		fprintf(f, "font %s\n", font->path);
		fprintf(f, "	mtime %lu\n", (unsigned long)font->mtime);
		fprintf(f, "	is_font %d\n", font->is_font);
		if (font->is_font) {
			fprintf(f, "	family %s\n", font->family_name);
			fprintf(f, "	style %s\n", font->style_name);
		}
		fprintf(f, "end font\n\n");
	}
	fclose(f);

	return rename(tmp, cache_path);
}

int ttf_map_file(htsp_t *map, const char *path, time_t mtime, void **cookie)
{
	FT_Library *lib = *cookie;
	FT_Face face;
	ttf_map_font_t *f = ttf_map_get(map, path, 1);

	if (mtime <= f->mtime)
		return 0;


	if ((FT_New_Face(*lib, path, 0, &face) == 0) && (face->family_name != NULL) && (face->style_name != NULL)) {
		f->is_font = 1;
		f->family_name = ttf_strdup(face->family_name);
		f->style_name = ttf_strdup(face->style_name);
		FT_Done_Face(face);
	}
	else
		f->is_font = 0;

	f->mtime = mtime;

	return 1;
}


int ttf_map_dir(htsp_t *map, const char *font_dir, void **cookie)
{
	DIR *dir = opendir(font_dir);
	struct dirent *de;
	struct stat st;
	char path[TTF_PATH_MAX+4], *fn; /* +4 to make sure we can't smash the stack on an off-by-one error */
	long len, maxlen;
	int res = 0;

	if (dir == NULL)
		return 0;

	/* prepare path: load font_dir, append / and remember where to append file name */
	len = strlen(font_dir);
	memcpy(path, font_dir, len);
	fn = path + len;
	*fn = '/';
	fn++;
	maxlen = TTF_PATH_MAX - len - 2; /* one for /, one for \0 */

	for(de = readdir(dir); de != NULL; de = readdir(dir)) {
		long len;

		if (*de->d_name == '.') continue; /* Ignore ., .. and hidden files */

		len = strlen(de->d_name);
		if (len > maxlen) continue; /* ignore files with full path too long; avoids buffer overrun */
		memcpy(fn, de->d_name, len+1);

		if (stat(path, &st) != 0) continue;
		if (S_ISDIR(st.st_mode))
			res |= ttf_map_dir(map, path, cookie);
		else
			res |= ttf_map_file(map, path, st.st_mtime, cookie);
	}

	closedir(dir);
	return res;
}

int ttf_map_dirs(htsp_t *map, const char **font_dirs, void **cookie)
{
	int res = 0;

	for(; *font_dirs != NULL; font_dirs++)
		res |= ttf_map_dir(map, *font_dirs, cookie);

	return res;
}

htsp_t *ttf_map_load(const char *cache_path, const char **font_dirs, void **cookie)
{
	htsp_t *map;
	FT_Library *lib = calloc(sizeof(FT_Library), 1);
	int res;

	if (lib == NULL)
		return NULL;

	if (FT_Init_FreeType(lib) != 0) {
		free(lib);
		return NULL;
	}

	*cookie = lib;

	map = htsp_alloc(strhash, strkeyeq);
	ttf_map_cache_load(map, cache_path);
	res = ttf_map_dirs(map, font_dirs, cookie);
	if (res > 0)
		ttf_map_cache_save(map, cache_path);

	return map;
}

void ttf_map_free(htsp_t *map, void **cookie)
{
	FT_Library *lib = *cookie;
	htsp_entry_t *e;

	for(e = htsp_first(map); e != NULL; e = htsp_next(map, e)) {
		ttf_map_font_t *f = e->value;
		free((char *)f->path);
		free((char *)f->family_name);
		free((char *)f->style_name);
		free(f);
	}

	htsp_free(map);

	FT_Done_FreeType(*lib);
	free(lib);
}
