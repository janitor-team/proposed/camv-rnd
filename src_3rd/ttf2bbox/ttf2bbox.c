#include <string.h>
#include <math.h>
#include <assert.h>
#include "ttf2bbox.h"

#if 0
#define deb_printf printf
#else
void deb_printf(const char *f, ...) {}
#endif

int ttf2bbox_y_mirror = 1;

int ttf2bbox_init(ttf2bbox_t *lib)
{
	memset(lib, 0, sizeof(ttf2bbox_t));
	if (FT_Init_FreeType(&lib->lib) != 0)
		return -1;
	return 0;
}

int ttf2bbox_uninit(ttf2bbox_t *lib)
{
	int res = 0, n;

	for(n = 0; n < lib->faces_used; n++)
		FT_Done_Face(lib->faces[n]);
	free(lib->faces);

	if (FT_Done_FreeType(lib->lib) != 0)
		res = -1;

	return res;
}

int ttf2bbox_font_load(ttf2bbox_t *lib, const char *path)
{
	int res;
	FT_Face *nf;

	if (lib->faces_used >= lib->faces_alloced) {
		lib->faces_alloced += 32;
		nf = realloc(lib->faces, sizeof(FT_Face) * lib->faces_alloced);
		if (nf == NULL)
			return -1;
		lib->faces = nf;
	}
	nf = &lib->faces[lib->faces_used];
	res = FT_New_Face(lib->lib, path, 0, nf);
	if (res != 0)
		return -1;

	return lib->faces_used++;;
}


ttf2bbox_pixmap_t *ttf2bbox_pm_alloc(long sx, long sy)
{
	ttf2bbox_pixmap_t *pm = calloc(sizeof(ttf2bbox_pixmap_t) + sx * sy - 1, 1);
	pm->sx = sx;
	pm->sy = sy;
	return pm;
}

static void draw_bitmap(ttf2bbox_pixmap_t *dst, FT_Bitmap *bitmap, FT_Int ox, FT_Int oy, int trx, int try, int ymirror)
{
	FT_Int x, y, p, q, w;
	FT_Int x_max = ox + bitmap->width;
 	FT_Int y_max = oy + bitmap->rows;

	assert(bitmap->pixel_mode == FT_PIXEL_MODE_GRAY);

	for(x = ox, p = 0; x < x_max; x++, p++) {
		int xx = x + trx;
		for(y = oy, q = 0; y < y_max; y++, q++) {
			int yy;
			if (ymirror) /* mirror y because of the strange coord system librnd has */
				yy = (dst->sy - y - 1) + try;
			else
				yy = y + try;

			if ((xx < 0)  || (yy < 0) || (xx >= dst->sx) || (yy >= dst->sy))
				continue;
			w = q * bitmap->width;
			dst->p[yy*dst->sx + xx] |= bitmap->buffer[w + p];
		}
	}
}

static int estimate(double *width, double *height, double scale, FT_Face face, FT_Matrix matrix, const char *text)
{
	FT_Vector pen;
	long int height_ = 0;
	FT_GlyphSlot slot = face->glyph;
	const char *s;

	if (FT_Set_Char_Size(face, scale, 0, 100, 0) != 0)
		return -1;

	pen.x = pen.y = 0;

	FT_Set_Transform(face, &matrix, &pen);
	FT_Load_Char(face, 'M', 0);
	height_ = slot->metrics.height;

	FT_Set_Transform(face, &matrix, &pen);
	FT_Load_Char(face, 'y', 0);
	height_ += slot->metrics.height - slot->metrics.horiBearingY;

	for(s = text; *s != '\0'; s++) 
	{
		FT_Set_Transform(face, &matrix, &pen);
		if (FT_Load_Char(face, *s, 0) != 0)
			continue;
		pen.x += slot->advance.x;
		pen.y += slot->advance.y;
	}

	*width = (double)pen.x / 64.0;
	*height = (double)height_ / 64.0;
	return 0;
}

static int tune(double *scale, long *cx, long *cy, long *boxw, long *boxh, double sx, double sy, FT_Face face, FT_Matrix matrix, const char *text, double angle, int bbox4rotated)
{
	double scx, scy, width, height, sa, ca;
	int i;

	if (angle != 0) {
		sa = sin(angle);
		ca = cos(angle);
	}
	else {
		sa = 0.0;
		ca = 1.0;
		bbox4rotated = 0;
	}

	for(i = 0; i < 10; i++) {
		if (estimate(&width, &height, *scale, face, matrix, text) < 0)
			return -1;

		deb_printf("bbox1: %f %f sc=%f;%f scale=%f sx;sy=%d;%d\n", width, height, scx, scy, *scale, (int)sx, (int)sy);

		if (bbox4rotated) {
			double bbh, bbw;
			bbw = width * ca + height * sa;
			bbh = width * sa + height * ca;
			*cx = bbw/2;
			*cy = bbh/2;
			*boxw = bbw;
			*boxh = bbh;
			scx = sx * (*scale) / bbw;
			scy = sy * (*scale) / bbh;
		}
		else {
			scx = (*scale) * sx / width;
			scy = (*scale) * sy / height;
			*cx = sx/2;
			*cy = sy/2;
			*boxw = width;
			*boxh = height;
		}

		if ((width <= sx) && (height <= sy))
			break;

		if (scx < scy)
			*scale = scx;
		else
			*scale = scy;

		deb_printf("bbox2: sc=%f;%f scale=%f\n", scx, scy, *scale);
	}


	deb_printf("bbox3: %f %f\n", width, height);


	return 0;
}

static void load_mx_rot(FT_Matrix *matrix, double angle)
{
	matrix->xx = (FT_Fixed)(+cos(angle) * 0x10000L);
	matrix->xy = (FT_Fixed)(-sin(angle) * 0x10000L);
	matrix->yx = (FT_Fixed)(+sin(angle) * 0x10000L);
	matrix->yy = (FT_Fixed)(+cos(angle) * 0x10000L);
}

static void load_mx_0(FT_Matrix *matrix)
{
	matrix->xx = (FT_Fixed)(+1 * 0x10000L);
	matrix->xy = (FT_Fixed)(-0 * 0x10000L);
	matrix->yx = (FT_Fixed)(+0 * 0x10000L);
	matrix->yy = (FT_Fixed)(+1 * 0x10000L);
}

int ttf2bbox_render_in_box(ttf2bbox_t *lib, int fid, ttf2bbox_pixmap_t *dst, const char *text, long ox, long oy, double angle, long sx, long sy, int bbox4rotated)
{
	FT_Matrix matrix;
	FT_Vector pen;
	FT_GlyphSlot slot;
	FT_Face face;
	FT_Pos bltune;
	const char *s;
	long int h, height = 0, tx, ty, drx = 0, dry = 0, cx, cy, boxw, boxh;
	double scale = 10000;

	if ((fid < 0) || (fid >= lib->faces_used))
		return -1;

	face = lib->faces[fid];
	slot = face->glyph;

	load_mx_0(&matrix);

	tune(&scale, &cx, &cy, &boxw, &boxh, sx, sy, face, matrix, text, angle, bbox4rotated);
deb_printf("c: %ld %ld box: %ld %ld s: %ld %ld\n", cx, cy, boxw, boxh, sx, sy);

	if (FT_Set_Char_Size(face, scale, 0, 100, 0) != 0)
		return -1;
	height = 0;

	load_mx_rot(&matrix, angle);
	if ((angle != 0) && bbox4rotated) {
		pen.x = (ox + sx/2 - cx) * 64;
		pen.y = (dst->sy - oy - sy - (cy/2)*sin(angle)) * 64+1;
	}
	else {
		tx = (sx - boxw)*64/2;
		ty = (sy - boxh)*64/2;
		pen.x = ox * 64 + tx;
		pen.y = (dst->sy - oy - sy) * 64 + ty+1;
	}


	FT_Set_Transform(face, &matrix, &pen);
	FT_Load_Char(face, 'y', FT_LOAD_RENDER);
	bltune = (slot->metrics.height - slot->metrics.horiBearingY)/64;

	for(s = text; *s != '\0'; s++) 
	{
		FT_Set_Transform(face, &matrix, &pen);
		if (FT_Load_Char(face, *s, FT_LOAD_RENDER) != 0)
			continue;

		draw_bitmap(dst, &slot->bitmap, slot->bitmap_left, dst->sy - slot->bitmap_top - bltune, drx, dry, ttf2bbox_y_mirror);
		pen.x += slot->advance.x;
		pen.y += slot->advance.y;
		h = slot->metrics.height;
		if (h > height)
			height = h;
	}

	deb_printf("res = %ld %ld / %ld %ld scale=%f\n", (pen.x)/64-ox, height/64, sx, sy, scale);
	return 0;
}

static void box_bump(long *x1, long *y1, long *x2, long *y2, long x, long y)
{
	if (x < *x1) *x1 = x;
	if (x > *x2) *x2 = x;
	if (y < *y1) *y1 = y;
	if (y > *y2) *y2 = y;
}

int ttf2bbox_estimate_at(ttf2bbox_t *lib, int fid, int size, const char *text, long ox, long oy, double angle, ttf2bbox_anchor_t hor, ttf2bbox_anchor_t ver, long *tox, long *toy, long *sx, long *sy, int bbox4rotated)
{
	FT_Matrix matrix;
	FT_Face face;
	double width, height, scale = size * 64, ca, sa;


	if ((fid < 0) || (fid >= lib->faces_used))
		return -1;

	face = lib->faces[fid];

	load_mx_0(&matrix);

	if (estimate(&width, &height, scale, face, matrix, text) != 0)
		return -1;

	ca = cos(angle);
	sa = sin(angle);

	*tox = ox;
	*toy = oy;

	switch(hor) {
		case TTF2BBOX_LEFT: break;
		case TTF2BBOX_MID:   *tox -= width/2 * ca; *toy -= width/2 * sa; break;
		case TTF2BBOX_RIGHT: *tox -= width * ca;   *toy -= width * sa; break;
	}

	switch(ver) {
		case TTF2BBOX_TOP: break;
		case TTF2BBOX_MID:    *toy -= height/2 * ca; *tox -= height/2 * ca; break;
		case TTF2BBOX_BOTTOM: *toy -= height * ca;   *tox -= height * ca; break;
	}

	if (bbox4rotated) {
		long x1, y1, x2, y2;

		/* *tox;*toy is the top left corner in neutral position rotated around ox;oy */
		x1 = x2 = *tox;
		y1 = y2 = *toy;
		box_bump(&x1, &y1, &x2, &y2, (*tox) + width * ca, (*toy) + width * sa);
		box_bump(&x1, &y1, &x2, &y2, (*tox) + height * sa, (*toy) + height * ca);
		box_bump(&x1, &y1, &x2, &y2, (*tox) + width * ca + height * sa, (*toy) + width * sa + height * ca);

		*tox = x1;
		*toy = y1;
		*sx = x2-x1;
		*sy = y2-y1;
	}
	else {
		*sx = width;
		*sy = height;
	}
	return 0;
}

