#include <ft2build.h>
#include FT_FREETYPE_H

typedef struct {
	FT_Library lib;
	FT_Face *faces;
	int faces_used, faces_alloced;
} ttf2bbox_t;

typedef struct {
	long sx, sy;
	unsigned char p[1];
} ttf2bbox_pixmap_t;

typedef enum {
	TTF2BBOX_LEFT = 1,
	TTF2BBOX_TOP = 1,
	TTF2BBOX_MID = 0,
	TTF2BBOX_RIGHT = 2,
	TTF2BBOX_BOTTOM = 2
} ttf2bbox_anchor_t;

int ttf2bbox_init(ttf2bbox_t *lib);
int ttf2bbox_uninit(ttf2bbox_t *lib);

int ttf2bbox_font_load(ttf2bbox_t *lib, const char *path);

ttf2bbox_pixmap_t *ttf2bbox_pm_alloc(long sx, long sy);

/* Render text in a way it fits within a bounding box; ox;oy is top left of the
   box of size sx*sy.
   If bbox4rotated is 0, the bounding box refers to the horizontally rendered
   version, which is then rotated around the bottom left corner. If it is
   non-zero, the bounding box is assumed to refer to the rotated version thus
   the text is rendered rotated and centered in the bounding box */
int ttf2bbox_render_in_box(ttf2bbox_t *lib, int fid, ttf2bbox_pixmap_t *dst, const char *text, long ox, long oy, double angle, long sx, long sy, int bbox4rotated);

/* Estimate the bounding box of a text of a given size placed at ox;oy. What
   part of the text ox;oy refers to is determined by hor;ver. Returns
   top left x and y coords in tox;toy and size of the box in sx;sy */
int ttf2bbox_estimate_at(ttf2bbox_t *lib, int fid, int size, const char *text, long ox, long oy, double angle, ttf2bbox_anchor_t hor, ttf2bbox_anchor_t ver, long *tox, long *toy, long *sx, long *sy, int bbox4rotated);


extern int ttf2bbox_y_mirror; /* 1 if need to mirror y dir; default is 1 */
