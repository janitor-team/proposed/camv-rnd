#include <stdio.h>
#include "ttf2bbox.h"

static const char *fontfile = "/usr/share/fonts/truetype/open-sans/OpenSans-Italic.ttf";
static ttf2bbox_t lib;
static const char color[] = " .+#";

static void draw_rect(ttf2bbox_pixmap_t *pm, unsigned char clr, int x1, int y1, int x2, int y2)
{
	int n;
	unsigned char *s1, *s2;

	s1 = pm->p + pm->sx * y1 + x1;
	s2 = pm->p + pm->sx * y2 + x1;
	for(n = x1; n <= x2; n++,s1++,s2++) {
		*s1 = clr;
		*s2 = clr;
	}

	s1 = pm->p + pm->sx * y1 + x1;
	s2 = pm->p + pm->sx * y1 + x2;
	for(n = y1; n <= y2; n++,s1+=pm->sx,s2+=pm->sx) {
		*s1 = clr;
		*s2 = clr;
	}
}

static void draw(FILE *f, ttf2bbox_pixmap_t *pm)
{
	unsigned char *s = pm->p;
	int n, end = pm->sx * pm->sy + 1;
	for(n = 1; n < end; n++,s++) {
		fputc(color[*s >> 6], f);
		if ((n % pm->sx) == 0)
			fputc('\n', f);
	}
}

int main(int argc, char *argv[])
{
	int fid, ox = 10, oy = 4, w = 50, h = 13, bbox4rotated=0;
	double rot = 0;
	const char *text = "hello world!";
	ttf2bbox_pixmap_t *pm;


	if (argc > 1)
		text = argv[1];

	if (argc > 2)
		rot = strtod(argv[2], NULL) / 180.0 * 3.141592654;

	if (ttf2bbox_init(&lib) != 0) {
		fprintf(stderr, "ttf2bbox_init() error\n");
		return 1;
	}

	fid = ttf2bbox_font_load(&lib, fontfile);
	if (fid < 0) {
		fprintf(stderr, "ttf2bbox_font_load() error\n");
		return 1;
	}

	pm = ttf2bbox_pm_alloc(80, 20);
	draw_rect(pm, 64, ox, oy, w+ox, h+oy);
	if (ttf2bbox_render_in_box(&lib, fid, pm, text, ox, oy, rot, w, h, bbox4rotated) != 0) {
		fprintf(stderr, "ttf2bbox_render() error\n");
		return 1;
	}
	draw(stdout, pm);

	if (ttf2bbox_uninit(&lib) != 0) {
		fprintf(stderr, "ttf2bbox_uninit() error\n");
		return 1;
	}


	return 0;
}
