#include <stdlib.h>
#include "ttf_map.h"

const char *font_dirs[] = {"/usr/share/fonts/truetype", NULL};

int main(int argc, char *argv[])
{
	void *cookie;
	htsp_t *map = ttf_map_load(".ttf_map.cache", font_dirs, &cookie);

	ttf_map_free(map, &cookie);
}

