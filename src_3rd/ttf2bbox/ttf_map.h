#include <time.h>
#include <genht/htsp.h>

typedef struct ttf_map_font_s ttf_map_font_t;

struct ttf_map_font_s {
	const char *path; /* also the hash key */
	time_t mtime;
	const char *family_name;
	const char *style_name;
	unsigned is_font:1;

	/* fields that can be used by the caller */
	long handle;
	unsigned handle_valid:1;
};

/* Should be loaded with something unique (e.g. PID); this avoids temp file
   race between two processes trying to update the same cache file */
extern char ttf_map_cache_uniq[8];

/* Loads and updates cache file at cache_path using all files in NULL terminated
   font_dirs. Returns the head of a hash table of malloc'd font info structs.
   Cookie is address to a void * used by the lib in the mapping. */
htsp_t *ttf_map_load(const char *cache_path, const char **font_dirs, void **cookie);

/* Frees a hash table of font info structs returned by ttf_map_load. Cookie
   is the same that was passed to ttf_map_load() */
void ttf_map_free(htsp_t *map, void **cookie);
