all: FORCE
	cd src && $(MAKE) all
# do not compile doc/ (all rendered files are commited - Ringdove policy)

install: FORCE
	cd src && $(MAKE) install
	cd doc && $(MAKE) install

linstall: FORCE
	cd src && $(MAKE) linstall
	cd doc && $(MAKE) linstall

uninstall: FORCE
	cd src && $(MAKE) uninstall
	cd doc && $(MAKE) uninstall

clean: FORCE
	cd src && $(MAKE) clean
# do not clean doc/ (all rendered files are commited - Ringdove policy)

distclean: FORCE
	cd src && $(MAKE) distclean
# do not clean doc/ (all rendered files are commited - Ringdove policy)

FORCE:
