
#include <librnd/config.h>

#include <string.h>

#include <librnd/core/global_typedefs.h>
#include <librnd/core/hidlib.h>
#include <librnd/core/tool.h>

static const char std_tools_cookie[] = "std_tools";

#include "tool_mline.c"
#include "tool_mobj.c"
#include "tool_arrow.c"

int pplg_check_ver_std_tools(int ver_needed) { return 0; }

void pplg_uninit_std_tools(void)
{
}

int pplg_init_std_tools(void)
{
	rnd_tool_reg(&camv_rnd_tool_arrow, std_tools_cookie);
	rnd_tool_reg(&camv_rnd_tool_mline, std_tools_cookie);
	rnd_tool_reg(&camv_rnd_tool_mobj, std_tools_cookie);

	return 0;
}
