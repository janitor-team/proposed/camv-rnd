void camv_layersel_gui_init_ev(rnd_hidlib_t *hidlib, void *user_data, int argc, rnd_event_arg_t argv[]);
void camv_layersel_vis_chg_ev(rnd_hidlib_t *hidlib, void *user_data, int argc, rnd_event_arg_t argv[]);
void camv_layersel_layer_chg_ev(rnd_hidlib_t *hidlib, void *user_data, int argc, rnd_event_arg_t argv[]);


extern const char camv_acts_Layer[];
extern const char camv_acth_Layer[];
fgw_error_t camv_act_Layer(fgw_arg_t *res, int argc, fgw_arg_t *argv);
