/*
 *                            COPYRIGHT
 *
 *  camv-rnd - electronics-related CAM viewer
 *  Copyright (C) 2019,2020 Tibor 'Igor2' Palinkas
 *  (copied from pcb-rnd by the author)
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 *  Contact:
 *    Project page: http://repo.hu/projects/camv-rnd
 *    lead developer: http://repo.hu/projects/camv-rnd/contact.html
 *    mailing list: camv-rnd (at) list.repo.hu (send "subscribe")
 */

#include "config.h"

#include <librnd/config.h>

#include <string.h>

#include "event.h"
#include <librnd/core/actions.h>
#include <librnd/core/hidlib.h>
#include <librnd/core/hidlib_conf.h>
#include <librnd/core/compat_misc.h>
#include <librnd/core/compat_fs.h>
#include <librnd/core/globalconst.h>
#include <librnd/core/conf_hid.h>

#include <librnd/plugins/lib_hid_common/dlg_export.h>

#include "data.h"
#include "plug_io_act.h"

#include "layersel.h"
#include "status.h"

static const char *layersel_cookie = "camv_gui/layersel";
static const char *status_cookie = "camv_gui/status";
static const char *status_cookie2 = "camv_gui/status2";
static const char *camv_gui_cookie = "camv_gui";

#define NOGUI() \
do { \
	if ((rnd_gui == NULL) || (!rnd_gui->gui)) { \
		RND_ACT_IRES(1); \
		return 0; \
	} \
	RND_ACT_IRES(0); \
} while(0)

const char camv_acts_Popup[] = "Popup(MenuName, [obj-type])";
const char camv_acth_Popup[] = "Bring up the popup menu specified by MenuName, optionally modified with the object type under the cursor.\n";
fgw_error_t camv_act_Popup(fgw_arg_t *res, int argc, fgw_arg_t *argv)
{
	char name[256], name2[256];
	const char *tn = NULL, *a0, *a1 = NULL;
	int r = 1;
	enum {
		CTX_NONE,
		CTX_OBJ_TYPE
	} ctx_sens = CTX_NONE;

	NOGUI();

	if (argc != 2 && argc != 3)
		RND_ACT_FAIL(Popup);

	RND_ACT_CONVARG(1, FGW_STR, Popup, a0 = argv[1].val.str);
	RND_ACT_MAY_CONVARG(2, FGW_STR, Popup, a1 = argv[2].val.str);

	*name = '\0';
	*name2 = '\0';

	if (argc == 3) {
		if (strcmp(a1, "obj-type") == 0) ctx_sens = CTX_OBJ_TYPE;
	}

	if (strlen(a0) < sizeof(name) - 32) {
		switch(ctx_sens) {
			case CTX_OBJ_TYPE:
				{
					rnd_coord_t x, y;
/*					pcb_objtype_t type;
					void *o1, *o2, *o3;*/
					rnd_hid_get_coords("context sensitive popup: select object", &x, &y, 0);
#if 0
					type = pcb_search_screen(x, y, PCB_OBJ_PSTK | PCB_OBJ_SUBC_PART, &o1, &o2, &o3);
					if (type == 0)
						type = pcb_search_screen(x, y, PCB_OBJ_CLASS_REAL, &o1, &o2, &o3);

					if (type == 0)
						tn = "none";
					else
						tn = pcb_obj_type_name(type);
#endif
					sprintf(name, "/popups/%s-%s", a0, tn);
					sprintf(name2, "/popups/%s-misc", a0);
				}
				break;
			case CTX_NONE:
				sprintf(name, "/popups/%s", a0);
				break;
				
		}
	}

	if (*name != '\0')
		r = rnd_gui->open_popup(rnd_gui, name);
	if ((r != 0) && (*name2 != '\0'))
		r = rnd_gui->open_popup(rnd_gui, name2);

	RND_ACT_IRES(r);
	return 0;
}

static char *dup_cwd(void)
{
	char tmp[RND_PATH_MAX + 1];
	return rnd_strdup(rnd_get_wd(tmp));
}

static const char camv_acts_Load[] = "Load()\n" "Load(Project|Layer)";
static const char camv_acth_Load[] = "Load a camv project or a layer from a user-selected file.";
static fgw_error_t camv_act_Load(fgw_arg_t *res, int argc, fgw_arg_t *argv)
{
	static char *last_project = NULL, *last_layer = NULL;
	const char *function = "Layer";
	char *name = NULL;

	if (last_layer == NULL)     last_layer = dup_cwd();
	if (last_project == NULL)   last_project = dup_cwd();

	/* Called with both function and file name -> no gui */
	if (argc > 2)
		return RND_ACT_CALL_C(RND_ACT_HIDLIB, camv_act_LoadFrom, res, argc, argv);

	RND_ACT_MAY_CONVARG(1, FGW_STR, Load, function = argv[1].val.str);

	if (rnd_strcasecmp(function, "Layer") == 0)
		name = rnd_gui->fileselect(rnd_gui, "Load layer", "Import a layer from file", last_layer, NULL, NULL, "layer", RND_HID_FSD_READ, NULL);
	else if (rnd_strcasecmp(function, "Project") == 0)
		name = rnd_gui->fileselect(rnd_gui, "Load a project file", "load project (all layers) from file", last_project, ".lht", NULL, "project", RND_HID_FSD_READ, NULL);
	else {
		rnd_message(RND_MSG_ERROR, "Invalid subcommand for Load(): '%s'\n", function);
		RND_ACT_IRES(1);
		return 0;
	}

	if (name != NULL) {
		if (rnd_conf.rc.verbose)
			fprintf(stderr, "Load:  Calling LoadFrom(%s, %s)\n", function, name);
		rnd_actionl("LoadFrom", function, name, NULL);
		free(name);
	}

	RND_ACT_IRES(0);
	return 0;
}

static rnd_action_t camv_gui_action_list[] = {
	{"PrintGUI", rnd_act_PrintDialog, rnd_acth_PrintDialog, rnd_acts_PrintDialog},
	{"Popup", camv_act_Popup, camv_acth_Popup, camv_acts_Popup},
	{"Load", camv_act_Load, camv_acth_Load, camv_acts_Load},
	{"Layer", camv_act_Layer, camv_acth_Layer, camv_acts_Layer},
	{"StatusSetText", camv_act_StatusSetText, camv_acth_StatusSetText, camv_acts_StatusSetText}
};

int pplg_check_ver_gui(int ver_needed) { return 0; }

void pplg_uninit_gui(void)
{
	rnd_actionl("rnd_toolbar_uninit", NULL);
	rnd_event_unbind_allcookie(status_cookie);
	rnd_conf_hid_unreg(status_cookie);
	rnd_conf_hid_unreg(status_cookie2);
}

static rnd_conf_hid_id_t install_events(const char *cookie, const char *paths[], rnd_conf_hid_callbacks_t cb[], void (*update_cb)(rnd_conf_native_t*,int))
{
	const char **rp;
	rnd_conf_native_t *nat;
	int n;
	rnd_conf_hid_id_t conf_id;

	conf_id = rnd_conf_hid_reg(cookie, NULL);
	for(rp = paths, n = 0; *rp != NULL; rp++, n++) {
		memset(&cb[n], 0, sizeof(cb[0]));
		cb[n].val_change_post = update_cb;
		nat = rnd_conf_get_field(*rp);
		if (nat != NULL)
			rnd_conf_hid_set_cb(nat, conf_id, &cb[n]);
	}

	return conf_id;
}

int pplg_init_gui(void)
{
	const char *stpaths[] = { "editor/grid_unit", "editor/grid", NULL };
	const char *rdpaths[] = { "editor/grid_unit", NULL };
	static rnd_conf_hid_callbacks_t stcb[sizeof(stpaths)/sizeof(stpaths[0])];
	static rnd_conf_hid_callbacks_t rdcb[sizeof(rdpaths)/sizeof(rdpaths[0])];

	rnd_event_bind(RND_EVENT_GUI_INIT, camv_layersel_gui_init_ev, NULL, layersel_cookie);
	rnd_event_bind(CAMV_EVENT_LAYERS_CHANGED, camv_layersel_layer_chg_ev, NULL, layersel_cookie);
	rnd_event_bind(RND_EVENT_GUI_INIT, camv_status_gui_init_ev, NULL, status_cookie);
	rnd_event_bind(RND_EVENT_USER_INPUT_KEY, camv_status_st_update_ev, NULL, status_cookie);
	rnd_event_bind(RND_EVENT_CROSSHAIR_MOVE, camv_status_rd_update_ev, NULL, status_cookie);

	install_events(status_cookie, stpaths, stcb, camv_status_st_update_conf);
	install_events(status_cookie2, rdpaths, rdcb, camv_status_rd_update_conf);

	RND_REGISTER_ACTIONS(camv_gui_action_list, camv_gui_cookie);
	rnd_actionl("rnd_toolbar_init", NULL);
	return 0;
}
