/*
 *                            COPYRIGHT
 *
 *  camv-rnd - electronics-related CAM viewer
 *  Copyright (C) 2022 Tibor 'Igor2' Palinkas
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 *  Contact:
 *    Project page: http://repo.hu/projects/camv-rnd
 *    lead developer: http://repo.hu/projects/camv-rnd/contact.html
 *    mailing list: camv-rnd (at) list.repo.hu (send "subscribe")
 */

#include "config.h"
#include <librnd/config.h>

#include <string.h>

#include <librnd/core/event.h>
#include <librnd/core/actions.h>
#include <librnd/core/hidlib.h>
#include <librnd/core/hidlib_conf.h>
#include <librnd/core/compat_misc.h>
#include <librnd/core/compat_fs.h>
#include <librnd/core/globalconst.h>
#include <librnd/core/plugins.h>
#include <librnd/plugins/lib_hid_common/dialogs_conf.h>
#include <librnd/plugins/lib_hid_common/dlg_pref.h>

#if LIBRND_3_2_0
#include <librnd/plugins/lib_hid_common/dlg_export.h>
#endif

#include "event.h"

#include "dlg_about.h"
#include "dlg_layer.h"

static const char *camv_dialogs_cookie = "camv_dialogs";

void camv_dialogs_layer_chg_ev(rnd_hidlib_t *hidlib, void *user_data, int argc, rnd_event_arg_t argv[])
{
	camv_layer_dlg_layer_chg_ev(hidlib, user_data, argc, argv);
}

void camv_dialogs_layer_selected_ev(rnd_hidlib_t *hidlib, void *user_data, int argc, rnd_event_arg_t argv[])
{
	camv_layer_dlg_layer_chg_ev(hidlib, user_data, argc, argv);
}


static rnd_action_t camv_dialogs_action_list[] = {
#if LIBRND_3_2_0
	{"PrintGUI", rnd_act_PrintDialog, rnd_acth_PrintDialog, rnd_acts_PrintDialog},
#endif
	{"About", camv_act_About, camv_acth_About, camv_acts_About},
	{"LayerDialog", camv_act_LayerDialog, camv_acth_LayerDialog, camv_acts_LayerDialog}
};

extern int pcb_dlg_pref_tab;
extern void (*pcb_dlg_pref_first_init)(pref_ctx_t *ctx, int tab);

int pplg_check_ver_dialogs(int ver_needed) { return 0; }

void pplg_uninit_dialogs(void)
{
	rnd_event_unbind_allcookie(camv_dialogs_cookie);
	rnd_remove_actions_by_cookie(camv_dialogs_cookie);
	rnd_dlg_pref_uninit();
}

int pplg_init_dialogs(void)
{
	RND_API_CHK_VER;

	RND_REGISTER_ACTIONS(camv_dialogs_action_list, camv_dialogs_cookie);
	rnd_dlg_pref_init(pcb_dlg_pref_tab, pcb_dlg_pref_first_init);

	rnd_event_bind(CAMV_EVENT_LAYERS_CHANGED, camv_dialogs_layer_chg_ev, NULL, camv_dialogs_cookie);
	rnd_event_bind(CAMV_EVENT_LAYER_SELECTED, camv_dialogs_layer_selected_ev, NULL, camv_dialogs_cookie);

	return 0;
}
