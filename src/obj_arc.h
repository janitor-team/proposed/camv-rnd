/*
 *                            COPYRIGHT
 *
 *  camv-rnd - electronics-related CAM viewer
 *  Copyright (C) 2019 Tibor 'Igor2' Palinkas
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 *  Contact:
 *    Project page: http://repo.hu/projects/camv-rnd
 *    lead developer: http://repo.hu/projects/camv-rnd/contact.html
 *    mailing list: camv-rnd (at) list.repo.hu (send "subscribe")
 */

#ifndef CAMV_OBJ_ARC_H
#define CAMV_OBJ_ARC_H

#include "obj_common.h"

typedef struct camv_arc_s {
	CAMV_ANY_PRIMITIVE_FIELDS;
	rnd_coord_t cx, cy; /* center */
	rnd_coord_t r;      /* radius */
	rnd_coord_t thick;
	rnd_angle_t start, delta;
} camv_arc_t;

void camv_arc_init(camv_arc_t *arc);
camv_arc_t *camv_arc_new(void);

#endif
