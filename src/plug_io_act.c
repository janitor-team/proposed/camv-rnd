/*
 *                            COPYRIGHT
 *
 *  camv-rnd - electronics-related CAM viewer
 *  Copyright (C) 2019 Tibor 'Igor2' Palinkas
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 *  Contact:
 *    Project page: http://repo.hu/projects/camv-rnd
 *    lead developer: http://repo.hu/projects/camv-rnd/contact.html
 *    mailing list: camv-rnd (at) list.repo.hu (send "subscribe")
 */

#include "config.h"

#include <librnd/config.h>

#include <librnd/core/actions.h>
#include <librnd/core/compat_misc.h>
#include "event.h"

#include "data.h"
#include "plug_io.h"
#include "plug_io_act.h"

static const char *plug_io_cookie = "plug_io_act";

static const char camv_acts_LoadFrom[] = "LoadFrom(Layer|Project,filename[,format])";
static const char camv_acth_LoadFrom[] = "Load project or layer data from a file.";
fgw_error_t camv_act_LoadFrom(fgw_arg_t *res, int argc, fgw_arg_t *argv)
{
	const char *op, *name, *format = NULL;

	RND_ACT_CONVARG(1, FGW_STR, LoadFrom, op = argv[1].val.str);
	RND_ACT_CONVARG(2, FGW_STR, LoadFrom, name = argv[2].val.str);
	RND_ACT_MAY_CONVARG(3, FGW_STR, LoadFrom, format = argv[3].val.str);

	if (rnd_strcasecmp(op, "layer") == 0) {
		if (camv_io_load(&camv, name) != 0) {
			rnd_message(RND_MSG_ERROR, "Can not load file '%s'\n", name);
			RND_ACT_IRES(-1);
			return 0;
		}
		rnd_event(&camv.hidlib, CAMV_EVENT_LAYERS_CHANGED, NULL);
	}
	else if (rnd_strcasecmp(op, "project") == 0) {
		TODO("the actual project load");
		rnd_message(RND_MSG_ERROR, "LoadFrom(project,...) not yet implemented\n");
	}
	else
		RND_ACT_FAIL(LoadFrom);

	RND_ACT_IRES(0);
	return 0;
}

static rnd_action_t camv_plug_io_act_list[] = {
	{"LoadFrom", camv_act_LoadFrom, camv_acth_LoadFrom, camv_acts_LoadFrom}
};

void camv_plug_io_act_init(void)
{
	RND_REGISTER_ACTIONS(camv_plug_io_act_list, plug_io_cookie);
}
