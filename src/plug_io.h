/*
 *                            COPYRIGHT
 *
 *  camv-rnd - electronics-related CAM viewer
 *  Copyright (C) 2019 Tibor 'Igor2' Palinkas
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 *  Contact:
 *    Project page: http://repo.hu/projects/camv-rnd
 *    lead developer: http://repo.hu/projects/camv-rnd/contact.html
 *    mailing list: camv-rnd (at) list.repo.hu (send "subscribe")
 */

#ifndef CAMV_RND_PLUG_IO
#define CAMV_RND_PLUG_IO

#include <stdio.h>
#include "data.h"

typedef struct camv_io_s {
	const char *name;
	int prio; /* the higher the better */
	int (*test_load)(camv_design_t *camv, const char *fn, FILE *f); /* returns non-zero if the file looks good for the plugin */
	int (*load)(camv_design_t *camv, const char *fn, FILE *f);
	int (*save_design)(camv_design_t *camv, const char *fn);
} camv_io_t;

void camv_io_reg(camv_io_t *io);
void camv_io_unreg(camv_io_t *io);
int camv_io_load(camv_design_t *camv, const char *fn);

#endif
