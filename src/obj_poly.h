/*
 *                            COPYRIGHT
 *
 *  camv-rnd - electronics-related CAM viewer
 *  Copyright (C) 2019 Tibor 'Igor2' Palinkas
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 *  Contact:
 *    Project page: http://repo.hu/projects/camv-rnd
 *    lead developer: http://repo.hu/projects/camv-rnd/contact.html
 *    mailing list: camv-rnd (at) list.repo.hu (send "subscribe")
 */

#ifndef CAMV_OBJ_POLY_H
#define CAMV_OBJ_POLY_H

#include "obj_common.h"
#include <librnd/core/vtc0.h>

typedef struct camv_poly_s {
	CAMV_ANY_PRIMITIVE_FIELDS;

	rnd_coord_t *x;
	rnd_coord_t *y;
	rnd_cardinal_t len;

	/* internal */
	vtc0_t points; /* single allocation for both x and y; first all x coords, then all y coords; as large as ->len*2 */
} camv_poly_t;

void camv_poly_init(camv_poly_t *poly);
camv_poly_t *camv_poly_new(void);

void camv_poly_allocpts(camv_poly_t *poly, rnd_cardinal_t len);

/*** internal ***/
int camv_poly_isc_box(const camv_any_obj_t *o, const camv_rtree_box_t *box); /* used by text */


#endif
