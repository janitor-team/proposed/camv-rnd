/*
 *                            COPYRIGHT
 *
 *  camv-rnd - electronics-related CAM viewer
 *  Copyright (C) 2019,2020 Tibor 'Igor2' Palinkas
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 *  Contact:
 *    Project page: http://repo.hu/projects/camv-rnd
 *    lead developer: http://repo.hu/projects/camv-rnd/contact.html
 *    mailing list: camv-rnd (at) list.repo.hu (send "subscribe")
 */

#include "config.h"

#include <librnd/config.h>
#include <librnd/core/actions.h>
#include <librnd/core/compat_misc.h>

#include "build_run.h"
#include "data.h"

static const char camv_acts_Quit[] = "Quit()";
static const char camv_acth_Quit[] = "Quits the application.";
static fgw_error_t camv_act_Quit(fgw_arg_t *res, int argc, fgw_arg_t *argv)
{
	camv_rnd_quit_app();
	return FGW_SUCCESS;
}

extern fgw_error_t rnd_gui_act_zoom(fgw_arg_t *res, int argc, fgw_arg_t *argv);

TODO("Figure how to avoid copying this from librnd rnd_gui_acts_zoom")
const char camv_acts_Zoom[] = \
	"Zoom()\n" \
	"Zoom([+|-|=]factor)\n" \
	"Zoom(x1, y1, x2, y2)\n" \
	"Zoom(?)\n" \
	"Zoom(get)\n" \
	"Zoom(auto_first)\n";
const char camv_acth_Zoom[] = "GUI zoom";
fgw_error_t camv_act_Zoom(fgw_arg_t *res, int argc, fgw_arg_t *argv)
{
	if ((argv[1].type & FGW_STR) == FGW_STR) {
		if (strcmp(argv[1].val.str, "auto_first") == 0) { /* zoom to extent when the first file is loaded (called from the menu file) */
			if (camv.layers.used == 1)
				return rnd_actionv_bin(RND_ACT_HIDLIB, "rnd_zoom", res, 0, argv);
			return 0;
		}
	}
	return rnd_actionv_bin(RND_ACT_HIDLIB, "rnd_zoom", res, argc, argv);
}

static rnd_action_t gui_action_list[] = {
	{"Quit", camv_act_Quit, camv_acth_Quit, camv_acts_Quit},
	{"Zoom", camv_act_Zoom, camv_acth_Zoom, camv_acts_Zoom},
};

void gui_act_init(void)
{
	RND_REGISTER_ACTIONS(gui_action_list, NULL)
}

