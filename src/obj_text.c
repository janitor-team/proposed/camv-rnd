/*
 *                            COPYRIGHT
 *
 *  camv-rnd - electronics-related CAM viewer
 *  Copyright (C) 2020 Tibor 'Igor2' Palinkas
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 *  Contact:
 *    Project page: http://repo.hu/projects/camv-rnd
 *    lead developer: http://repo.hu/projects/camv-rnd/contact.html
 *    mailing list: camv-rnd (at) list.repo.hu (send "subscribe")
 */

#include <librnd/config.h>

#include "obj_text.h"
#include "obj_any.h"
#include "conf_core.h"

#include "geo.h"
#include <gengeo2d/box.h>
#include "obj_poly.h"

#include <librnd/core/hid_inlines.h>
#include <librnd/core/compat_misc.h>
#include <librnd/core/hidlib_conf.h>
#include "ttf2bbox/ttf2bbox.h"

static ttf2bbox_t ttf;
static int def_fid, ttf_inited = 0;

static void camv_text_free_fields(camv_any_obj_t *obj)
{
	if (obj->text.pm != NULL) {
		if (rnd_render->uninit_pixmap != NULL)
			rnd_render->uninit_pixmap(rnd_render, obj->text.pm);
		rnd_pixmap_free(obj->text.pm);
		obj->text.pm = NULL;
	}
	free(obj->text.s);
	obj->text.s = NULL;
}

static void camv_text_draw(camv_any_obj_t *obj, rnd_hid_gc_t gc)
{
	if ((rnd_render->draw_pixmap != NULL) && (obj->text.pm != NULL))
		rnd_render->draw_pixmap(rnd_render, obj->text.x, obj->text.y, obj->text.sx, obj->text.sy, obj->text.pm);
}

static void camv_text_bbox(camv_any_obj_t *obj)
{
	rnd_coord_t sx2 = obj->text.sx / 2, sy2 = obj->text.sy / 2;

	obj->text.bbox.x1 = obj->text.x - sx2;
	obj->text.bbox.x2 = obj->text.x + sx2;
	obj->text.bbox.y1 = obj->text.y - sy2;
	obj->text.bbox.y2 = obj->text.y + sy2;
	obj->text.bbox_valid = 1;
}

void camv_text_update_color(camv_text_t *text, camv_layer_t *ly)
{
	long n, end = text->pm->sx * text->pm->sy;
	unsigned char *i, *o;
	ttf2bbox_pixmap_t *tpm = text->ttf_pm;

	if (ly == NULL)
		ly = text->parent_layer;

	text->pm->tr = text->pm->tg = 127;
	text->pm->tb = 128;
	text->pm->has_transp = 1;


	for(n = 0, i = tpm->p, o = text->pm->p; n < end; n++, i++, o+=3) {
		if (i[0] == 0) {
			o[0] = text->pm->tr;
			o[1] = text->pm->tg;
			o[2] = text->pm->tb;
		}
		else {
			o[0] = ly->color.fr * i[0] + rnd_conf.appearance.color.background.fr * (255-i[0]);
			o[1] = ly->color.fg * i[0] + rnd_conf.appearance.color.background.fg * (255-i[0]);
			o[2] = ly->color.fb * i[0] + rnd_conf.appearance.color.background.fb * (255-i[0]);
		}
#if 0
		if ((n % text->pm->sx) == 0) printf("\n");
		printf("%d", i[0]/64);
#endif
	}
}

void camv_text_update(rnd_hidlib_t *hidlib, camv_text_t *text, camv_layer_t *ly)
{
	long tox, toy, sx, sy;


	if (!ttf_inited) {
		if (ttf2bbox_init(&ttf) != 0) {
			fprintf(stderr, "Failed to initialize ttf2bbox\n"); /* shuld never happen */
			exit(1);
		}
		def_fid = ttf2bbox_font_load(&ttf, conf_core.appearance.default_font);
		if (def_fid < 0) {
			rnd_message(RND_MSG_ERROR, "Failed to load ttf: '%s'\nPlease configure appearance/default_font to point to a ttf!\n", conf_core.appearance.default_font);
			return;
		}
		ttf_inited = 1;
	}

	if (ttf2bbox_estimate_at(&ttf, def_fid, text->size, text->s, 0, 0, text->rot, TTF2BBOX_LEFT, TTF2BBOX_TOP, &tox, &toy, &sx, &sy, 1) < 0) {
		fprintf(stderr, "Failed to estimate text size with ttf2bbox\n"); /* shuld never happen */
		exit(1);
	}
	rnd_trace("text estimate: t=%ld;%ld s=%ld;%ld\n", tox, toy, sx, sy);
	if (text->ttf_pm != NULL)
		free(text->ttf_pm);
	text->ttf_pm = ttf2bbox_pm_alloc(sx, sy);
	ttf2bbox_render_in_box(&ttf, def_fid, text->ttf_pm, text->s, 0, 0, text->rot, sx, sy, 1);
	text->sx = RND_MM_TO_COORD(sx)/128;
	text->sy = RND_MM_TO_COORD(sy)/128;

	if (text->pm != NULL)
		rnd_pixmap_free(text->pm);
	text->pm = rnd_pixmap_alloc(hidlib, sx, sy);
	camv_text_update_color(text, ly); /* really copies ->ttf_pm to ->pm */
}

static void camv_text_copy(camv_any_obj_t *dst, const camv_any_obj_t *src)
{
	memcpy(&dst->text, &src->text, sizeof(camv_text_t));
	dst->text.s = rnd_strdup(src->text.s);
}

static void camv_text_move(camv_any_obj_t *o, rnd_coord_t dx, rnd_coord_t dy)
{
	o->text.x += dx;
	o->text.y += dy;
	if (o->text.bbox_valid) {
		o->text.bbox.x1 += dx; o->text.bbox.y1 += dy;
		o->text.bbox.x2 += dx; o->text.bbox.y2 += dy;
	}
}

static camv_any_obj_t *camv_text_alloc(void) { return (camv_any_obj_t *)camv_text_new(); }

static int camv_text_isc_box(const camv_any_obj_t *o, const camv_rtree_box_t *box)
{
	camv_any_obj_t op;
	rnd_coord_t xy[8];

	op.poly.points.alloced = op.poly.points.used = 8;
	op.poly.points.array = xy;
	op.poly.x = xy;
	op.poly.y = xy+4;

	return 0;

	TODO("geo: copy the code from pcb-rnd for calculating the corners; need to move mx tranform code from pcb-rnd code to librnd");
	return camv_poly_isc_box(&op, box);
}

static const camv_objcalls_t camv_text_calls = {
	camv_text_alloc,
	camv_text_free_fields,
	camv_text_draw,
	camv_text_bbox,
	camv_text_copy,
	camv_text_move,
	camv_text_isc_box
};

void camv_text_init(camv_text_t *text)
{
	memset(text, 0, sizeof(camv_text_t));
	text->type = CAMV_OBJ_TEXT;
	text->calls = &camv_text_calls;
}

camv_text_t *camv_text_new(void)
{
	camv_text_t *res = malloc(sizeof(camv_text_t));
	camv_text_init(res);
	return res;
}

