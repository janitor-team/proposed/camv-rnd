ha:rnd-menu-v1 {
	li:mouse {
		li:left {
			li:press            = { Mode(Notify) }
			li:press-shift      = { Mode(Notify) }
			li:press-ctrl       = { Mode(Notify) }
			li:release          = { Mode(Release) }
			li:release-shift    = { Mode(Release) }
			li:release-ctrl     = { Mode(Release) }
		}
		li:middle {
			li:press              = { Pan(1) }
			li:release            = { Pan(0) }
		}
		li:right {
			li:press         = { Mode(Stroke) }
			li:release       = { Mode(Release); Popup(popup-obj, obj-type) }
			li:shift-release = { Popup(popup-obj-misc) }
			li:press-ctrl    = { Display(CycleCrosshair) }
		}
		li:scroll-up {
			li:press        = { Zoom(0.8) }
			li:press-shift  = { Scroll(up) }
			li:press-ctrl   = { Scroll(left) }
		}
		li:scroll-down {
			li:press       = { Zoom(1.25) }
			li:press-shift = { Scroll(down) }
			li:press-ctrl  = { Scroll(right) }
		}
	}

	# List of tool names for the toolbar.
	# Do not specify keys here, they are specified in the menu; the GUI will
	# not pick up keys from this subtree. There's also no action here: the
	# plugin handling this subtree will always pick the tool.
	li:toolbar_static {
		ha:mline     {tip={linear measurement}}
		ha:mobj      {tip={object measurement (prints to message log)}}
		ha:arrow     {tip={switch to arrow mode}}
	}

	li:main_menu {
		### File Menu
		ha:File {
			li:submenu {
				ha:New                  = { li:a={{<key>f;<key>n}; {Ctrl<Key>n};}; action=Layer(DelAll) }
				ha:Load...              = { a={<key>f;<key>o}; action={Load(); Zoom(auto_first);} }
				-
				ha:Loader configuration {
					li:submenu {
						ha:Assume laser cut on g-code = { checked=plugins/import_gcode/laser; action=conf(toggle, plugins/import_gcode/laser, design) }
					}
				}
				-
				ha:Print drawing...     = { a={<key>f;<key>p}; action=Print()}
				ha:Export drawing...    = { a={<key>f;<key>e}; action=ExportDialog()}
				-
				ha:Preferences...       = { a={<key>i;<key>c;<key>p}; action=preferences}
				-
				ha:Quit                 = { a={<key>f;<key>q};          action=Quit() }
			}
		}

		ha:View {
			li:submenu {
			ha:Grid {
				li:submenu {
					ha:Enable visible grid        = { a={<key>g; <key>v}; checked=editor/draw_grid; action=conf(toggle, editor/draw_grid, design) }
					ha:Enable local grid          = { a={<key>g; <key>l}; checked=plugins/hid_gtk/local_grid/enable; action=conf(toggle, plugins/hid_gtk/local_grid/enable, design) }
					ha:Grid units {
						li:submenu {
							ha:mil                    = { a={<key>g; <key>i}; checked=ChkGridUnits(mil); action=SetUnits(mil); update_on={editor/grid_unit} }
							ha:mm                     = { a={<key>g; <key>m}; checked=ChkGridUnits(mm); action=SetUnits(mm); update_on={editor/grid_unit} }
						}
					}
					ha:Grid size = {
						li:submenu {
							ha:No Grid                = { checked=ChkGridSize(none); action=SetGrid(1); update_on={editor/grid} }
							-
							@grid
							-
							ha:Previous grid          = { li:a={{<key>g; <key>b};{<char>[};}; action=Grid(down) }
							ha:Next grid              = { li:a={{<key>g; <key>f};{<char>]};}; action=Grid(up) }
							-
							ha:Grid *2                = { a={<key>g; <key>d}; action=SetGrid(*2) }
							ha:Grid /2                = { a={<key>g; <key>h}; action=SetGrid(/2) }
							ha:Grid -5mil             = { action=SetGrid(-5,mil) }
							ha:Grid +5mil             = { action=SetGrid(+5,mil) }
							ha:Grid -0.05mm           = { action=SetGrid(-0.05,mm) }
							ha:Grid +0.05mm           = { action=SetGrid(+0.05,mm) }
						}
					}
					ha:Grid properties = {
						li:submenu {
							ha:Enable local grid      = { checked=plugins/hid_gtk/local_grid/enable; action=conf(toggle, plugins/hid_gtk/local_grid/enable, design) }
							-
							ha:local grid radius 4    = { checked={conf(iseq, plugins/hid_gtk/local_grid/radius, 4)}; li:action={conf(set, plugins/hid_gtk/local_grid/radius, 4, design); conf(set, plugins/hid_gtk/local_grid/enable, 1, design) } update_on={} }
							ha:local grid radius 8    = { checked={conf(iseq, plugins/hid_gtk/local_grid/radius, 8)}; li:action={conf(set, plugins/hid_gtk/local_grid/radius, 8, design); conf(set, plugins/hid_gtk/local_grid/enable, 1, design) } update_on={} }
							ha:local grid radius 16   = { checked={conf(iseq, plugins/hid_gtk/local_grid/radius, 16)}; li:action={conf(set, plugins/hid_gtk/local_grid/radius, 16, design); conf(set, plugins/hid_gtk/local_grid/enable, 1, design) } update_on={} }
							ha:local grid radius 32   = { checked={conf(iseq, plugins/hid_gtk/local_grid/radius, 32)}; li:action={conf(set, plugins/hid_gtk/local_grid/radius, 32, design); conf(set, plugins/hid_gtk/local_grid/enable, 1, design) } update_on={} }
							-
							ha:sparse global grid     = { checked=plugins/hid_gtk/global_grid/sparse; action=conf(toggle, plugins/hid_gtk/global_grid/sparse, design) }
							ha:global grid density 4  = { checked={conf(iseq, plugins/hid_gtk/global_grid/min_dist_px, 4)}; li:action={conf(set, plugins/hid_gtk/global_grid/min_dist_px, 4, design); conf(set, plugins/hid_gtk/local_grid/enable, 0, design) } update_on={} }
							ha:global grid density 8  = { checked={conf(iseq, plugins/hid_gtk/global_grid/min_dist_px, 8)}; li:action={conf(set, plugins/hid_gtk/global_grid/min_dist_px, 8, design); conf(set, plugins/hid_gtk/local_grid/enable, 0, design) } update_on={} }
							ha:global grid density 16 = { checked={conf(iseq, plugins/hid_gtk/global_grid/min_dist_px, 16)}; li:action={conf(set, plugins/hid_gtk/global_grid/min_dist_px, 16, design); conf(set, plugins/hid_gtk/local_grid/enable, 0, design) } update_on={} }
						}
					}
					ha:Realign grid               = { a={<key>g; <key>r}; action={ GetXY(Click to set the grid origin); Display(ToggleGrid) } }
					}
				}
				ha:Current layer {
					li:submenu {
						ha:Move up          { li:a={{<Key>l;<Key>u}; {<Key>l;<Key>Up};};        action=Layer(up) }
						ha:Move down        { li:a={{<Key>l;<Key>d}; {<Key>l;<Key>Down};};      action=Layer(down) }
						ha:Move to top      { li:a={{<Key>l;<Key>t}; {<Key>l;<Key>Page_Up};};   action=Layer(top) }
						ha:Move to bottom   { li:a={{<Key>l;<Key>b}; {<Key>l;<Key>Page_Down};}; action=Layer(bottom) }
						ha:Add from file... { li:a={{<Key>l;<Key>a}; {<Key>l;<Key>+};};         action=Load(Layer) }
						ha:Remove           { li:a={{<Key>l;<Key>r}; {<Key>l;<Key>-};};         action=Layer(del) }
					}
				}
				ha:Zoom {
					li:submenu {
						ha:Zoom In 20%            = { li:a={{<Key>z;<Key>z;}; {<char>+};} action=Zoom(-1.2) }
						ha:Zoom Out 20%           = { li:a={{<Key>z;<Key>x;}; {<char>-};} action=Zoom(+1.2) }
						ha:Zoom In 2X             = { action=Zoom(-2) }
						ha:Zoom Out 2X            = { action=Zoom(+2) }
						ha:Zoom to 0.1mil/px      = { action={Zoom(=0.1mil)} }
						ha:Zoom to 0.01mm/px      = { action={Zoom(=0.01mm)} }
						ha:Zoom to 1mil/px        = { action={Zoom(=1mil)} }
						ha:Zoom to 0.05mm/px      = { action={Zoom(=0.05mm)} }
						ha:Zoom to 2.5mil/px      = { action={Zoom(=2.5mil)} }
						ha:Zoom to 0.1mm/px       = { action={Zoom(=0.1mm)} }
						ha:Zoom to 10mil/px       = { action={Zoom(=10mil)} }
						ha:Zoom In 20% and center = { li:action={Zoom(-1.2); Center()} }
						ha:Zoom Out 20% and center= { li:action={Zoom(+1.2); Center()} }
						-
						ha:Zoom Extents           = {      li:a={{<Key>z;<Key>e;}; {<key>v;<key>f};}; action=Zoom() }
						ha:Zoom to selection      = {      a={<Key>z;<Key>s;}; action=ZoomTo(selected) }
						ha:Zoom to found          = {      a={<Key>z;<Key>f;}; action=ZoomTo(found) }
						-
						ha:Center cursor          = { a={<key>v; <key>c} action=Center() }
					}
				}
			}
		}

		ha:Plugins {
			li:submenu {
				ha:feature plugins   = {
					# submenu dedicated for feature plugins to register their menu items
					li:submenu {
						@feature_plugins
					}
				}
				ha:script            = {
					# submenu dedicated for user scripts to register their menu items
					li:submenu {
						@scripts
					}
				}
				ha:Manage plugins... = { a={<Key>p;<Key>m;<Key>p;}; action=ManagePlugins() }
				ha:Manage scripts... = { a={<Key>p;<Key>m;<Key>s;}; action=BrowseScripts() }
			}
		} # Plugins

		ha:Window {
			li:submenu {
				ha:Message Log    = { a={<Key>w;<Key>m}; action=LogDialog() }
				ha:Command Entry  = { li:a={a={<Key>w;<Key>c}; {<char>:};}  action=Command() }
			}
		}

		ha:Help {
			li:submenu {
				ha:About                = { action=About() }
			}
		}
	} #main_menu

	li:popups {
# context sensitive right click: popup per object type under the cursor

		ha:layer {
			li:submenu {
				ha:Move up          { action=Layer(up) }
				ha:Move down        { action=Layer(down) }
				ha:Move to top      { action=Layer(top) }
				ha:Move to bottom   { action=Layer(bottom) }
				-
				ha:Add from file... { action=Load(Layer) }
				ha:Remove           { action=Layer(del) }
				-
				ha:Edit properties... { action=LayerDialog() }
			}
		}
	} #popups

} # root
