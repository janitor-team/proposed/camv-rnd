/*
 *                            COPYRIGHT
 *
 *  camv-rnd - electronics-related CAM viewer
 *  Copyright (C) 2020 Tibor 'Igor2' Palinkas
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 *  Contact:
 *    Project page: http://repo.hu/projects/camv-rnd
 *    lead developer: http://repo.hu/projects/camv-rnd/contact.html
 *    mailing list: camv-rnd (at) list.repo.hu (send "subscribe")
 */

#ifndef CAMV_EVENT_H
#define CAMV_EVENT_H

#include <librnd/core/event.h>

enum {
	CAMV_EVENT_LAYERS_CHANGED = RND_EVENT_app,   /* called after layers or layer groups change (used to be the LayersChanged action) */
	CAMV_EVENT_LAYERVIS_CHANGED,                 /* called after the visibility of layers has changed */
	CAMV_EVENT_LAYER_SELECTED,                   /* called when a new layer is selected; args: int layer_id */
	CAMV_EVENT_last                              /* not a real event */
};

void camv_event_init_app(void);

#endif
