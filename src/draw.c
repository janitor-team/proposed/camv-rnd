/*
 *                            COPYRIGHT
 *
 *  camv-rnd - electronics-related CAM viewer
 *  Copyright (C) 2019 Tibor 'Igor2' Palinkas
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 *  Contact:
 *    Project page: http://repo.hu/projects/camv-rnd
 *    lead developer: http://repo.hu/projects/camv-rnd/contact.html
 *    mailing list: camv-rnd (at) list.repo.hu (send "subscribe")
 */

#include <librnd/core/hid.h>
#include "obj_any.h"

void camv_draw_layer(camv_layer_t *ly, rnd_hid_gc_t gc, const rnd_hid_expose_ctx_t *region, rnd_xform_t *xform_caller)
{
	camv_rtree_it_t it;
	void *o;

	for(o = camv_rtree_first(&it, &ly->objs, (camv_rtree_box_t *)&region->view); o != NULL; o = camv_rtree_next(&it)) {
		camv_any_obj_t *obj = o;
		obj->proto.calls->draw(obj, gc);
	}
}

void camv_expose_main(rnd_hid_t *hid, const rnd_hid_expose_ctx_t *region, rnd_xform_t *xform_caller)
{
	rnd_cardinal_t lid;
	rnd_hid_gc_t gc;
	int clearing = -1, direct = 1, need_flush = 0;
	rnd_hid_t *save = rnd_render;
	
	if ((rnd_render != NULL) && (!rnd_render->override_render))
		rnd_render = hid;

	gc = rnd_render->make_gc(rnd_render);

	/* calculate whether the cheaper direct-draw mechanism can be done */
	for(lid = 0; lid < camv.layers.used; lid++) {
		camv_layer_t *ly = camv.layers.array[lid];
		if (!ly->vis) continue;
		if (ly->clearing) {
			direct = 0;
			break;
		}
	}

	/* announce start of rendering */
	rnd_render->render_burst(rnd_render, RND_HID_BURST_START, &region->view);

	/* the HID needs a group set, even if we don't do groups */
	if (rnd_render->set_layer_group != NULL)
		rnd_render->set_layer_group(rnd_render, 0, NULL, 0, 0, 0, 0, NULL);

	for(lid = 0; lid < camv.layers.used; lid++) {
		camv_layer_t *ly = camv.layers.array[lid];
		if (!ly->vis) continue;
		if (!ly->sub) {
			if (need_flush) {
				rnd_render->set_drawing_mode(rnd_render, RND_HID_COMP_FLUSH, direct, &region->view);
				rnd_render->end_layer(rnd_render);
			}
			rnd_render->set_drawing_mode(rnd_render, RND_HID_COMP_RESET, direct, &region->view);
			need_flush = 1;


		}

		if (((int)ly->clearing != clearing) || (!ly->sub)) {
			clearing = ly->clearing;
			rnd_render->set_drawing_mode(rnd_render, ly->clearing ? RND_HID_COMP_NEGATIVE : RND_HID_COMP_POSITIVE, direct, &region->view);
		}
		rnd_render->set_color(gc, &ly->color);
		camv_draw_layer(ly, gc, region, xform_caller);
	}

	if (need_flush) {
		rnd_render->set_drawing_mode(rnd_render, RND_HID_COMP_FLUSH, direct, &region->view);
		rnd_render->end_layer(rnd_render);
	}

	rnd_render->render_burst(rnd_render, RND_HID_BURST_END, &region->view);
	rnd_render->destroy_gc(gc);

	rnd_render = save;
}

void camv_expose_preview(rnd_hid_t *hid, const rnd_hid_expose_ctx_t *e)
{

}
