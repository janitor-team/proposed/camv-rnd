/*
 *                            COPYRIGHT
 *
 *  camv-rnd - electronics-related CAM viewer
 *  Copyright (C) 2019 Tibor 'Igor2' Palinkas
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 *  Contact:
 *    Project page: http://repo.hu/projects/camv-rnd
 *    lead developer: http://repo.hu/projects/camv-rnd/contact.html
 *    mailing list: camv-rnd (at) list.repo.hu (send "subscribe")
 */

#include <librnd/config.h>

#include <stdlib.h>
#include <genvector/vtp0.h>

#include <librnd/core/error.h>
#include <librnd/core/safe_fs.h>

#include "plug_io.h"


static vtp0_t camv_io;
static int camv_io_sorted = 0;

static int cmp_io_prio(const void *v1, const void *v2)
{
	const camv_io_t *io1 = v1, *io2 = v2;

	if (io1->prio > io2->prio)
		return 1;
	return -1;
}

static void sort_io(void)
{
	if (camv_io_sorted)
		return;
	qsort(camv_io.array, camv_io.used, sizeof(void *), cmp_io_prio);
	camv_io_sorted = 1;
}

void camv_io_reg(camv_io_t *io)
{
	vtp0_append(&camv_io, io);
	camv_io_sorted = 0;
}

void camv_io_unreg(camv_io_t *io)
{
	size_t n;
	for(n = 0; n < camv_io.used; n++) {
		if (camv_io.array[n] == io) {
			vtp0_remove(&camv_io, n , 1);
			return;
		}
	}
}

static void post_load(camv_design_t *camv)
{
	camv_data_bbox(camv);
	camv->hidlib.size_x = camv->bbox.x2;
	camv->hidlib.size_y = camv->bbox.y2;
}

int camv_io_load(camv_design_t *camv, const char *fn)
{
	const camv_io_t *io;
	int n;
	FILE *f;
	
	f = rnd_fopen(&camv->hidlib, fn, "rb");
	if (f == NULL) {
		rnd_message(RND_MSG_ERROR, "Can not open '%s' for read\n", fn);
		return -1;
	}

	sort_io();

	for(n = 0; n < camv_io.used; n++) {
		io = camv_io.array[n];
		if (io->load == NULL)
			continue;
		rewind(f);
		if ((io->test_load == NULL) || (io->test_load(camv, fn, f) != 0)) {
			rewind(f);
			if (io->load(camv, fn, f) == 0) {
				fclose(f);
				post_load(camv);
				return 0;
			}
		}
	}

	return -1;
}
