/*
 *                            COPYRIGHT
 *
 *  camv-rnd - electronics-related CAM viewer
 *  Copyright (C) 2021 Tibor 'Igor2' Palinkas
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 *  Contact:
 *    Project page: http://repo.hu/projects/camv-rnd
 *    lead developer: http://repo.hu/projects/camv-rnd/contact.html
 *    mailing list: camv-rnd (at) list.repo.hu (send "subscribe")
 */

#include "config.h"

#include <stdio.h>
#include "config.h"
#include <librnd/core/actions.h>
#include "conf_core.h"
#include <librnd/core/hid_init.h>
#include <librnd/core/compat_misc.h>
#include <librnd/core/safe_fs.h>
#include "data.h"
#include "build_run.h"

/* print usage lines */
static inline void u(const char *fmt, ...)
{
	va_list ap;
	va_start(ap, fmt);
	vfprintf(stderr, fmt, ap);
	fputc('\n', stderr);
	va_end(ap);
}

static const char camv_acts_PrintUsage[] =
	"PrintUsage()\n"
	"PrintUsage(plugin)";

static const char camv_acth_PrintUsage[] = "Print command line arguments of camv-rnd or a plugin loaded.";

static int help0(void)
{
	rnd_hid_t **hl = rnd_hid_enumerate();
	int i;

	u("camv-rnd PCB related CAM file viewer, http://repo.hu/projects/camv-rnd");
	u("For more information, please read the topic help pages:");
	u("  camv-rnd --help topic");
	u("Topics are:");
	u("  invocation           how to run camv-rnd");
	u("  main                 main/misc flags (affecting none or all plugins)");
	for (i = 0; hl[i]; i++)
		if (hl[i]->usage != NULL)
			u("  %-20s %s", hl[i]->name, hl[i]->description);
	return 0;
}

extern const char *camv_action_args[];
static int help_main(void) {
	const char **cs;
	for(cs = camv_action_args; cs[2] != NULL; cs += RND_ACTION_ARGS_WIDTH) {
		fprintf(stderr, "camv-rnd [");
		if (cs[0] != NULL)
			fprintf(stderr, "-%s", cs[0]);
		if ((cs[0] != NULL) && (cs[1] != NULL))
			fprintf(stderr, "|");
		if (cs[1] != NULL)
			fprintf(stderr, "-%s", cs[1]);
		fprintf(stderr, "]    %s\n", cs[3]);
	}
	return 0;
}

static int help_invoc(void)
{
	rnd_hid_t **hl = rnd_hid_enumerate();
	int i;
	int n_printer = 0, n_exporter = 0;

	u("camv-rnd invocation:");
	u("");
	u("camv-rnd [main options]                                    See --help main");
	u("");
	u("camv-rnd [generics] [--gui GUI] [gui options] <cam file>   interactive GUI");

	u("Available GUI HIDs:");
	rnd_hid_print_all_gui_plugins();

/*
	u("\ncamv-rnd [generics] -p [printing options] <cam file>\tto print");
	u("Available printing hid%s:", n_printer == 1 ? "" : "s");
	for (i = 0; hl[i]; i++)
		if (hl[i]->printer)
			fprintf(stderr, "\t%-8s %s\n", hl[i]->name, hl[i]->description);

	u("\ncamv-rnd [generics] -x hid [export options] <cam file>\tto export");
	u("Available export hid%s:", n_exporter == 1 ? "" : "s");
	for (i = 0; hl[i]; i++)
		if (hl[i]->exporter)
			fprintf(stderr, "\t%-8s %s\n", hl[i]->name, hl[i]->description);
*/

	u("\nGenerics:");
	u("-c conf/path=value        set the value of a configuration item (in RND_CFR_CLI)");
	u("-C conffile               load config file (as RND_CFR_CLI; after all -c's)");

	return 0;
}

fgw_error_t camv_act_PrintUsage(fgw_arg_t *res, int argc, fgw_arg_t *argv)
{
	const char *topic = NULL, *subt = NULL;
	RND_ACT_MAY_CONVARG(1, FGW_STR, PrintUsage, topic = argv[1].val.str);
	RND_ACT_IRES(0);

	u("");
	if (topic != NULL) {
		rnd_hid_t **hl = rnd_hid_enumerate();
		int i;

		if (strcmp(topic, "invocation") == 0)  return help_invoc();
		if (strcmp(topic, "main") == 0)        return help_main();

		for (i = 0; hl[i]; i++) {
			if ((hl[i]->usage != NULL) && (strcmp(topic, hl[i]->name) == 0)) {
				RND_ACT_MAY_CONVARG(2, FGW_STR, PrintUsage, subt = argv[2].val.str);
				RND_ACT_IRES(hl[i]->usage(hl[i], subt));
				return 0;
			}
		}
		fprintf(stderr, "No help available for %s\n", topic);
		RND_ACT_IRES(-1);
		return 0;
	}
	else
		help0();
	return 0;
}


static const char camv_acts_PrintVersion[] = "PrintVersion()";
static const char camv_acth_PrintVersion[] = "Print version.";
fgw_error_t camv_act_PrintVersion(fgw_arg_t *res, int argc, fgw_arg_t *argv)
{
	printf("%s\n", camv_rnd_get_info_program());
	RND_ACT_IRES(0);
	return 0;
}

static const char camv_acts_DumpVersion[] = "DumpVersion()";
static const char camv_acth_DumpVersion[] = "Dump version in script readable format.";
fgw_error_t camv_act_DumpVersion(fgw_arg_t *res, int argc, fgw_arg_t *argv)
{
	printf(CAMV_VERS "\n");
	RND_ACT_IRES(0);
	return 0;
}

static const char camv_acts_PrintCopyright[] = "PrintCopyright()";
static const char camv_acth_PrintCopyright[] = "Print copyright notice.";
fgw_error_t camv_act_PrintCopyright(fgw_arg_t *res, int argc, fgw_arg_t *argv)
{
	printf("camv-rnd - electronics-related CAM viewer\n");
	printf("Copyright (C) 2019..2021 Tibor 'Igor2' Palinkas\n");
	printf("    This program is free software; you can redistribute it and/or modify\n"
				 "    it under the terms of the GNU General Public License as published by\n"
				 "    the Free Software Foundation; either version 2 of the License, or\n"
				 "    (at your option) any later version.\n\n");
	printf("    This program is distributed in the hope that it will be useful,\n"
				 "    but WITHOUT ANY WARRANTY; without even the implied warranty of\n"
				 "    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\n"
				 "    GNU General Public License for more details.\n\n");
	printf("    You should have received a copy of the GNU General Public License\n"
				 "    along with this program; if not, write to the Free Software\n"
				 "    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.\n\n");
	RND_ACT_IRES(0);
	return 0;
}

static const char camv_acts_PrintPaths[] = "PrintPaths()";
static const char camv_acth_PrintPaths[] = "Print full paths and search paths.";
static void print_list(const rnd_conflist_t *cl)
{
	int n;
	rnd_conf_listitem_t *ci;
	const char *p;

	printf(" ");
	rnd_conf_loop_list_str(cl, ci, p, n) {
		printf("%c%s", (n == 0) ? '"' : ':', p);
	}
	printf("\"\n");
}
fgw_error_t camv_act_PrintPaths(fgw_arg_t *res, int argc, fgw_arg_t *argv)
{
	htsp_entry_t *e;
	rnd_conf_fields_foreach(e) {
		rnd_conf_native_t *n = e->value;
		if ((strncmp(n->hash_path, "rc/path/", 8) == 0) && (n->type == RND_CFN_STRING) && (n->used == 1))
			printf("%-32s = %s\n", n->hash_path, n->val.string[0]);
	}
	RND_ACT_IRES(0);
	return 0;
}

/* Note: this can not be in librnd because of the app-specific setenvs */
static const char camv_acts_System[] = "System(shell_cmd)";
static const char camv_acth_System[] = "Run shell command";
fgw_error_t camv_act_System(fgw_arg_t *res, int argc, fgw_arg_t *argv)
{
	char tmp[128];
	const char *cmd;

	RND_ACT_CONVARG(1, FGW_STR, System, cmd = argv[1].val.str);
	RND_ACT_IRES(0);

/*	rnd_setenv("CAMV_RND_LAYER_FILE_NAME", RND_ACT_HIDLIB->filename == NULL ? "" : RND_ACT_HIDLIB->filename, 1);*/
	rnd_snprintf(tmp, sizeof(tmp), "%mm", camv.crosshair_x);
	rnd_setenv("CAMV_RND_CROSSHAIR_X_MM", tmp, 1);
	rnd_snprintf(tmp, sizeof(tmp), "%mm", camv.crosshair_y);
	rnd_setenv("CAMV_RND_CROSSHAIR_Y_MM", tmp, 1);
	RND_ACT_IRES(rnd_system(RND_ACT_HIDLIB, cmd));
	return 0;
}

/* Note: this can not be in librnd because of potential app-specific things */
static const char camv_acts_ExecuteFile[] = "ExecuteFile(filename)";
static const char camv_acth_ExecuteFile[] = "Run actions from the given file.";
/* DOC: executefile.html */
fgw_error_t camv_act_ExecuteFile(fgw_arg_t *res, int argc, fgw_arg_t *argv)
{
	const char *fname;

	RND_ACT_CONVARG(1, FGW_STR, ExecuteFile, fname = argv[1].val.str);
	RND_ACT_IRES(rnd_act_execute_file(RND_ACT_HIDLIB, fname));
	return 0;
}


static rnd_action_t main_action_list[] = {
	{"PrintUsage", camv_act_PrintUsage, camv_acth_PrintUsage, camv_acts_PrintUsage},
	{"PrintVersion", camv_act_PrintVersion, camv_acth_PrintVersion, camv_acts_PrintVersion},
	{"DumpVersion", camv_act_DumpVersion, camv_acth_DumpVersion, camv_acts_DumpVersion},
	{"PrintCopyright", camv_act_PrintCopyright, camv_acth_PrintCopyright, camv_acts_PrintCopyright},
	{"PrintPaths", camv_act_PrintPaths, camv_acth_PrintPaths, camv_acts_PrintPaths},
	{"System", camv_act_System, camv_acth_System, camv_acts_System},
	{"ExecActionFile", camv_act_ExecuteFile, camv_acth_ExecuteFile, camv_acts_ExecuteFile}
};

void camv_main_act_init2(void)
{
	RND_REGISTER_ACTIONS(main_action_list, NULL);
}
