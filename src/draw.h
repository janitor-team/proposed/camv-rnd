typedef struct rnd_xform_s {
	int dummy;
} rnd_xform_t;

void camv_expose_main(rnd_hid_t *hid, const rnd_hid_expose_ctx_t *region, rnd_xform_t *xform_caller);
void camv_expose_preview(rnd_hid_t *hid, const rnd_hid_expose_ctx_t *e);
